package main

import (
	"bytes"
	"flag"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"time"

	bf "gopkg.in/russross/blackfriday.v2"

	latex "github.com/ambrevar/blackfriday-latex"
)

const (
	author           = "Pierre Neidhardt"
	defaultTitle     = author
	repo             = `https://bitbucket.org/ambrevar/ambrevar.bitbucket.org`
	repoComments     = repo + `/issues`
	repoPullRequests = repo + `/pull-requests`
	repoHistory      = repo + `/history-node/master/`
)

// Custom
func writeDocumentFooter(r *bf.HTMLRenderer, w *bytes.Buffer) {
	w.WriteString("\n<footer>")
	w.WriteString("\n  <a href=\"" + repoComments + `">Comments</a> – <a href="` + repoPullRequests + `">Pull requests</a><br/>`)
	if options.license != "" {
		w.WriteString("\n  <a href=\"" + options.license + "\">License</a>")
	}
	w.WriteString("\n  2014&ndash;" + fmt.Sprintf("%d", time.Now().Year()))
	if options.index != "" {
		w.WriteString("\n  <a href=\"" + options.index + `">` + author + "</a>")
	} else {
		w.WriteString(author + " ")
	}
	w.WriteString("\n  (ambrevar [at] gmail.com)")
	if options.pgpKey != "" {
		w.WriteString("\n  [<a href=\"" + options.pgpKey + "\">PGP key</a>]")
	}
	w.WriteString("\n</footer>")
	w.WriteString("\n</body>\n</html>\n")
}

// Custom: remove the Smartypants code and add the MathJax script
func writeDocumentHeader(r *bf.HTMLRenderer, w *bytes.Buffer, hasMath bool) {
	if r.Flags&bf.CompletePage == 0 {
		return
	}
	ending := ""
	if r.Flags&bf.UseXHTML != 0 {
		w.WriteString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" ")
		w.WriteString("\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n")
		w.WriteString("<html xmlns=\"http://www.w3.org/1999/xhtml\">\n")
		ending = " /"
	} else {
		w.WriteString("<!DOCTYPE html>\n")
		w.WriteString("<html>\n")
	}
	w.WriteString("<head>\n")
	w.WriteString("  <title>")
	// No smartypants for title.
	w.Write(esc([]byte(r.Title)))
	w.WriteString("</title>\n")
	w.WriteString("  <meta name=\"GENERATOR\" content=\"Blackfriday Markdown Processor v")
	w.WriteString(bf.Version)
	w.WriteString("\"")
	w.WriteString(ending)
	w.WriteString(">\n")
	w.WriteString("  <meta charset=\"utf-8\"")
	w.WriteString(ending)
	w.WriteString(">\n")
	if r.CSS != "" {
		w.WriteString("  <link rel=\"stylesheet\" type=\"text/css\" href=\"")
		w.Write(esc([]byte(r.CSS)))
		w.WriteString("\"")
		w.WriteString(ending)
		w.WriteString(">\n")
	}
	if r.Icon != "" {
		w.WriteString("  <link rel=\"icon\" type=\"image/x-icon\" href=\"")
		w.Write(esc([]byte(r.Icon)))
		w.WriteString("\"")
		w.WriteString(ending)
		w.WriteString(">\n")
	}
	if hasMath {
		w.WriteString(`  <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>`)
	}
	w.WriteString("</head>\n")
	w.WriteString("<body>\n\n")
}

// copy
func esc(text []byte) []byte {
	unesc := []byte(html.UnescapeString(string(text)))
	return escCode(unesc)
}

// copy
func escCode(text []byte) []byte {
	e1 := []byte(html.EscapeString(string(text)))
	e2 := bytes.Replace(e1, []byte("&#34;"), []byte("&quot;"), -1)
	return bytes.Replace(e2, []byte("&#39;"), []byte{'\''}, -1)
}

func mustRel(path string) string {
	if path != "" {
		var err error
		path, err = filepath.Rel(filepath.Dir(flag.Arg(0)), path)
		if err != nil {
			log.Fatal("error setting relative path:", err)
		}
	}
	return path
}

// copy except when noted
func writeTOC(r *bf.HTMLRenderer, w *bytes.Buffer, ast *bf.Node) {
	buf := bytes.Buffer{}

	inHeader := false
	tocLevel := 0
	headerCount := 0

	ast.Walk(func(node *bf.Node, entering bool) bf.WalkStatus {
		if node.Type == bf.Heading && !node.HeadingData.IsTitleblock {
			inHeader = entering
			if entering {
				node.HeadingID = fmt.Sprintf("toc_%d", headerCount)
				if node.Level == tocLevel {
					buf.WriteString("</li>\n\n<li>")
				} else if node.Level < tocLevel {
					for node.Level < tocLevel {
						tocLevel--
						buf.WriteString("</li>\n</ul>")
					}
					buf.WriteString("</li>\n\n<li>")
				} else {
					for node.Level > tocLevel {
						tocLevel++
						buf.WriteString("\n<ul>\n<li>")
					}
				}

				fmt.Fprintf(&buf, `<a href="#toc_%d">`, headerCount)
				headerCount++
			} else {
				buf.WriteString("</a>")
			}
			return bf.GoToNext
		}

		if inHeader {
			return r.RenderNode(&buf, node, entering)
		}

		return bf.GoToNext
	})

	for ; tocLevel > 0; tocLevel-- {
		buf.WriteString("</li>\n</ul>")
	}

	if buf.Len() > 0 {
		w.WriteString("<nav>\n")
		w.Write(buf.Bytes())
		w.WriteString("\n\n</nav>\n")
	}
	// Do not set r.lastOutputLen since it is private. If a nav section contains a
	// tag, this will insert a newline before the next node, that is before the
	// <h1> in the title.
}

func renderTitle(renderer *bf.HTMLRenderer, titleNode *bf.Node, output *bytes.Buffer, target string) bf.WalkStatus {
	output.WriteString("\n" + `<div class="title">`)

	var subtitleNode *bf.Node
	titleNode.Walk(func(c *bf.Node, entering bool) bf.WalkStatus {
		if subtitleNode != nil {
			// When subtitle has been found, do not render anything but the title closer.
			if c == titleNode {
				return renderer.RenderNode(output, c, entering)
			}
			return bf.GoToNext
		}
		if c.Type == bf.Text {
			if i := bytes.IndexByte(c.Literal, '\n'); i >= 0 {
				output.Write(c.Literal[:i])
				subtitleNode = c
				return bf.GoToNext
			}
		}
		return renderer.RenderNode(output, c, entering)
	})

	if subtitleNode != nil {
		i := bytes.IndexByte(subtitleNode.Literal, '\n')
		subtitleNode.Literal = subtitleNode.Literal[i+1:]
		output.WriteString(`<p id="subtitle">`)
		subtitleNode.Walk(func(c *bf.Node, entering bool) bf.WalkStatus {
			if c == titleNode && !entering {
				return bf.Terminate
			}
			if c.Type == bf.Text && bytes.IndexByte(c.Literal, '\n') >= 0 {
				c.Literal = bytes.Replace(c.Literal, []byte{'\n'}, []byte(" – "), -1)
			}
			return renderer.RenderNode(output, c, entering)
		})
		output.WriteString("</p>\n")
	}

	if options.date != "" {
		output.WriteString(`<p><a href="` + repoHistory + target + `">Updated ` + options.date + "</a></p>\n")
	}
	output.WriteString("</div>\n")
	return bf.SkipChildren
}

func renderHTML(renderer *bf.HTMLRenderer, ast *bf.Node, target string) []byte {
	var output bytes.Buffer

	hasMath := false
	ast.Walk(func(node *bf.Node, entering bool) bf.WalkStatus {
		switch node.Type {
		case bf.Code:
			if bytes.HasPrefix(node.Literal, []byte("$$ ")) {
				hasMath = true
				return bf.Terminate
			}
		case bf.CodeBlock:
			infoWords := bytes.Split(node.Info, []byte("\t "))
			if len(infoWords) > 0 && bytes.Equal(infoWords[0], []byte("math")) {
				hasMath = true
				return bf.Terminate
			}
		}
		return bf.GoToNext
	})

	writeDocumentHeader(renderer, &output, hasMath)
	if renderer.Flags&bf.TOC != 0 {
		writeTOC(renderer, &output, ast)
	}

	ast.Walk(func(node *bf.Node, entering bool) bf.WalkStatus {
		switch node.Type {
		case bf.Code:
			if bytes.HasPrefix(node.Literal, []byte("$$ ")) {
				output.Write([]byte("\\("))
				output.Write(escCode(node.Literal[3:]))
				output.Write([]byte("\\)"))
				return bf.GoToNext
			}
		case bf.CodeBlock:
			infoWords := bytes.Split(node.Info, []byte("\t "))
			if len(infoWords) > 0 && bytes.Equal(infoWords[0], []byte("math")) {
				output.Write([]byte("\\[\n"))
				output.Write(escCode(node.Literal))
				output.Write([]byte("\\]\n\n"))
				return bf.GoToNext
			}
		case bf.Heading:
			if node.HeadingData.IsTitleblock {
				return renderTitle(renderer, node, &output, target)
			}
		}

		return renderer.RenderNode(&output, node, entering)
	})

	writeDocumentFooter(renderer, &output)
	return output.Bytes()
}

var options struct {
	css     string
	date    string
	icon    string
	hasMath bool
	index   string
	latex   bool
	license string
	pgpKey  string
}

func main() {
	flag.StringVar(&options.css, "css", "", "Link to a CSS stylesheet")
	flag.StringVar(&options.date, "date", "", "Date of last update")
	flag.StringVar(&options.icon, "icon", "", "Set website icon")
	flag.StringVar(&options.index, "index", "", "Path to index page")
	flag.BoolVar(&options.latex, "latex", false, "Generate LaTeX output instead of HTML")
	flag.StringVar(&options.license, "license", "", "Path to license page")
	flag.StringVar(&options.pgpKey, "pgp", "", "Path to pgp key")

	flag.Parse()

	options.css = mustRel(options.css)
	options.icon = mustRel(options.icon)
	options.index = mustRel(options.index)
	options.license = mustRel(options.license)
	options.pgpKey = mustRel(options.pgpKey)

	// Read the input.
	var input []byte
	var err error
	args := flag.Args()
	switch len(args) {
	case 1, 2:
		if input, err = ioutil.ReadFile(args[0]); err != nil {
			log.Fatal("Error reading from", args[0], ":", err)
		}
	default:
		flag.Usage()
		return
	}

	// Set up options.
	extensions := bf.CommonExtensions | bf.Titleblock

	hasMainHeader, _ := regexp.Match(`(?m)^#\s`, input)

	md := bf.New(bf.WithExtensions(extensions))
	ast := md.Parse(input)

	// Set title: concatenate all Text children of Titleblock.
	title := defaultTitle
	ast.Walk(func(node *bf.Node, entering bool) bf.WalkStatus {
		if node.Type == bf.Heading && node.HeadingData.IsTitleblock {
			title = ""
			node.Walk(func(c *bf.Node, entering bool) bf.WalkStatus {
				if c.Type == bf.Text {
					if i := bytes.IndexByte(c.Literal, '\n'); i > -1 {
						title += string(c.Literal[:i])
						return bf.Terminate
					}
					title += string(c.Literal)
				}
				return bf.GoToNext
			})
			return bf.Terminate
		}
		return bf.GoToNext
	})

	var result []byte
	if options.latex {
		LaTeXFlags := latex.CompletePage
		if hasMainHeader {
			LaTeXFlags |= latex.TOC
		}
		renderer := &latex.Renderer{Author: author, Flags: LaTeXFlags | latex.SkipLinks}
		result = renderer.Render(ast)
	} else {
		HTMLFlags := bf.CommonHTMLFlags | bf.CompletePage
		if hasMainHeader {
			HTMLFlags |= bf.TOC
		}
		renderer := bf.NewHTMLRenderer(bf.HTMLRendererParameters{Title: title, CSS: options.css, Icon: options.icon, Flags: HTMLFlags})
		result = renderHTML(renderer, ast, args[0])
	}

	// Output the result.
	var out *os.File
	if len(args) == 2 {
		if out, err = os.Create(args[1]); err != nil {
			log.Fatalf("Error creating %s: %v", args[1], err)
		}
		defer out.Close()
	} else {
		out = os.Stdout
	}

	if _, err = out.Write(result); err != nil {
		log.Fatal("Error writing output:", err)
	}
}
