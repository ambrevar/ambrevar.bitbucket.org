.SUFFIXES: .md .pdf .asy

LATEXC ?= ./generate
LATEXFLAGS ?= -latex
PDFC ?= pdflatex

.PHONY: all
all: homogeneous/index.pdf

homogeneous/index.pdf: homogeneous/promotion.pdf homogeneous/perspective.pdf

${LATEXC}: generate.go
	go build -o $@

%.pdf: %.tex
	cd $$(dirname "$<") && ${PDFC} $$(basename "$<") && ${PDFC} $$(basename "$<") && rm \
	$$(basename "$*").aux $$(basename "$*").log $$(basename "$*").out $$(basename "$*").toc

%.tex: %.md ${LATEXC}
	${LATEXC} ${LATEXFLAGS} $< $@

%.pdf: %.asy
	cd $$(dirname "$<") && asy -f pdf $$(basename "$<") && rm $$(basename "$*")_.pbsdat texput.pbsdat
