% Power Apps

The following programs mostly target power users and programmers.  Recurring
characteristics: FOSS, lightweight, fast, versionable configuration (plain text,
non-XML), keyboard-driven, _Unix_ design.  Or simply an Emacs package.


#### 3D graphics: Blender

#### Archivers: atool, bzip2, cabextract, cdrtools, cpio, gzip, lha, lrzip, lz4, lzop, p7zip, unace, unarj, unrar, unshield, unzip, upx, xz

* `atool` is a wrapper around many tools to unify compression and extraction from
command-line.

* `cdrtools` can process ISO files.

* `xz` offers a good compression ratio together with great decompression
performance. Compression is quite demanding. `bzip2` has little to offer
compared to `xz`. `p7zip` bundles the features `tar`, `xz` and `ccrypt` in one
program.

* `lrzip` is at its best on large files and with multiple CPUs available.

* `upx` can compress executables.

* The rest is for decompression.

#### Assembly: NASM, Yasm

#### Audio editors: Audacity

#### Binary file tools: chrpath, DHEX, Emacs (nhexl-mode), ltrace, nm, od, strace

#### Calculation: bc, calc, Emacs (calc, Lisp), Maxima, PARI/GP, Octave

* `bc` and `calc` are simple arbitrary-precision calculators. bc is lighter but
calc has somewhat more features.

* `PARI/GP` is an extremely fast and advanced algebra system for number theory.
Great for prime numbers and such.

* `Octave` will serve as a Unix-designed Matlab.

#### Camera capture: Guvcview

#### Clipboard tools: xsel

Using [Emacs everywhere](../emacs-everywhere), xsel is barely useful.
I only use it to yank the filename of pictures from sxiv.

#### Contacts: Abook, Emacs (org-contacts)

* Abook is a stand-alone curses contact manager. Contacts are stored in plain
text and as such they are versionable, but the numbering makes up for huge
diffs.

* org-contacts is much more powerful thank Abook: free form, arbitrary fields,
contacts can be fuzzy-searched/retrieved from anywhere within Emacs.

Versioning your contacts is a great way to centralize them, instead of spreading
them across your mail agent contacts, CSV files, etc. Plain text contacts also
means it is easy to write a converter from a CSV file (e.g. using `AWK`).

#### Cryptography: ccrypt, encfs, GnuPG, pass, Pwgen

* `GnuPG` is overkill when it comes to encrypting files for personal use.

* `ccrypt` is good at encrypting single files.

* `encfs` can encrypt folders and mount them as an encrypted file system. Files
can be browsed transparently without being ever written in clear to the disk. It
uses the fuse backend which makes it portable across systems at the expense of
speed.

#### Dictionaries: aspell

#### Disk utilities: gparted, ncdu, parted, rmlint, shred, trash-cli, tree, udiskie, wipe

* `ncdu` is a very fast and convenient disk usage analyzer.

* `trash-cli` is a command-line interface implementing FreeDesktop.org's Trash
specification.  It can be combined with a file browser for easier use.  Emacs
also has the `delete-by-moving-to-trash` variable.

* `wipe` can delete folders securely while `shred` can only process files.

#### Document processing: groff, lilypond, Markdown, pdf2svg, Texinfo, TeX Live, txt2man

#### Document readers and tools: antiword, apvlv, catdvi, catdoc, docx2txt, Emacs (pdf-tools), evince, ghostscript, odt2txt, mcomix, poppler, pstotext, unrtf, wv, xchm, zathura

* `apvlv`, `pdf-tools` and `zathura` are light and keyboard-driven. `zathura`
supports PostScript .ps files and SyncTeX. `apvlv` supports UMD, HTML and .txt
files.  `pdf-tools` supports SyncTeX and has good text search and selection
facilities, it enables many Emacs features (Helm, Ivy).

* `Evince` is far too heavy but can fill PDF forms. (Yet another ill-conceived
PDF feature...)

* `Poppler` has numerous PDF converters (e.g. _pdftotext_).

* `ghostscript` can convert PDF to and from PS.

* Most of these programs can be used for previewing files from some
applications, e.g. `ranger`.

#### E-mail clients: Emacs (mu4e), Mutt

* Mutt is extensible but somewhat hard to configure.  Each e-mail account setup
requires a good deal of manual configuration.

* mu4e is moderately easy to configure, extremely extensible with Emacs Lisp and
suffers from far less limitations than Mutt.  You can fuzzy-search contacts,
e-mails, preview HTML, display embedded pictures and much more.

#### File browsers: Emacs (dired, Helm), ranger

* `ranger` is configurable and extensible. It can preview any kind of file in
the way you want. It can run an arbitrary command on any file selection and it
remembers the selection in every folder. It can run various powerful commands
conveniently, such as recursive hardlink creation or batch-renaming.

* Emacs Helm makes for a revolutionary file browser: typing anything will
fuzzy-filter the current directory, or even subdirectories.  The filtered
results can be browsed with special keys: you can select files from different
directories and apply arbitrary actions to them.

#### File synchronization: hsync, rsync

* Read the documentation carefully. `rsync` has a lot of useful options, like
`--append-verify`, `--progress` and `--size-only`.

#### Finders: Emacs (Helm, Ivy), fzf, tre

* `fzf` puts any shell on steroids. To such an extent it can replace any file
browser with no hesitation. It allows you to fuzzy-find anything: files, folders
to cd into, command history, folder history, completion...

* `tre` has `agrep`.

* If you live in Emacs, fzf and tre are completely superseded by Helm.

#### FTP clients: curlftpfs, Emacs (TRAMP), NcFTP

Emacs' TRAMP allows you to work on remote files (move, delete, download) and
edit them transparently: first they are automatically downloaded, then all edits
are done locally within Emacs, and last the file is uploaded upon saving.

#### FTP servers: vsftpd

#### Gaming emulators: DGen, DOSBox, Gens/GS, Mupen64Plus, PCSX-Reloaded, MAME, Wine

#### Gaming tools: DriConf, TiMidity++, xosd

* `osd_cat` from `xosd` can display text on screen, such as FPS or network
traffic.

#### Music: Beets, clyrics, cmus, Demlo, Emacs (EMMS), mps-youtube

* `clyrics` can display the lyrics of the song currently playing in `cmus`.

* `cmus` is extremely fast at updating the library. Its UI makes it convenient
to browse the library and to create playlists.

* Emacs' EMMS is similar to cmus with the bonus that it is extensible, it can
fetch lyrics, it can resume state upon restart and it can display album covers
within the music library tree.

* `mps-youtube` can build albums from Youtube links automatically and save the
resulting playlist locally.

#### Network monitors: Aircrack-ng, ngrep, nmap, Tcpdump, Wireshark

#### News readers: Emacs (elfeed), Newsbeuter

* Elfeed is better at rendering HTML feeds than newsbeuter, it supports image
display, it can fuzzy-search everything...

#### Picture batch processors: dcraw, gifsicle GraphicsMagick, ImageMagick, optipng

* `dcraw` can convert many camera raw formats.

* `GraphicsMagick` and `ImageMagick` are very similar in feature, and they may
be complementary regarding performance.

#### Picture editors: darktable, GIMP, Hugin, Metapixel, RawTherapee

#### Picture viewers: feh, sxiv

* `feh` can set the wallpaper. `sxiv` is very fast at loading and displaying big
pictures. It supports GIF animations unlike `feh`.

#### Programming: cloc

#### Programming in C: cppcheck, GDB, mcpp, musl, Splint, TCC, Valgrind, Uncrustify

* `cppcheck` and `Splint` are static analyzers with overlapping features.

* `Uncrustify` is much better engineered than `GNU Indent` and `Astyle`. See the
  [Indentation rationale](../indentation/index.html).

#### Screen capture: scrot

#### Session locks: slock, vlock, xlockmore, xss-lock

* `slock` is as simple as it can be but does not support PAM sessions unlike
`xlockmore`.

* `vlock` is for TTY sessions. It is part of the `kbd` project.

#### Shell: DASH, Emacs (Eshell), fish, shellcheck

* `DASH` is a light, fast and POSIX compliant shell. It is quite limited for
interactive use but ideal for testing the POSIX-ness of scripts.

* `fish` departs from the POSIX-derived shells. `Bash` suffers from the design
issues of the venerable Bourne shell (e.g. word-splitting). `Zsh` has tried to
unite all shell languages under one banner, thus becoming complicated beyond
reason to the point that the simplest configuration can be an Odyssey on its
own. Like `rc`, `fish` uses a clear syntax. It also has a straightforward API,
which makes it very straighforward to customize and extend. Last but not least,
its interactive features are efficient and to the point.

	The lack of POSIX-ness is no problem in practice:

	- Any POSIX shell script will be executed by the interpreter pointed by the
shebang.

	- Initialization files such as `.profile` can still be set up by `sh` at the
beginning of the session: use `sh` as your login shell and `exec fish` at the
end.

* Further down the road of non-POSIX shells, Eshell lies even further: it uses
Emacs Lisp as a language which is arguably much more powerful than `fish`.  See
[my article on Eshell](../emacs-eshell) for more good reasons why you should use
it.

	Eshell has a very powerful completion framework, `pcomplete`.  As of 2017 the
limited popularity of Eshell result in limited support for completion.  That
being said, it is possible to configure Eshell so that it falls back on the
completion of `fish`, which then makes for a very extensive completion support.

* `shellcheck` is a static-analyzer for shell scripts.

#### Spreadsheet: Emacs (Org-mode)

* This `Emacs` mode lets you write plain text tables (track them with `git`!)
and apply arbitrary functions to cells.  These functions are either pre-defined
or self-written in Lisp (with ubiquitous support for `M-x calc` and its
arbitrary precision arithmetic).  From there you can use every Elisp feature,
and if that would not be enough (e.g. too slow) you can call external programs
to perform the task, say, your favourite scripting language.  This makes the
tables infinitely programmable.

#### System monitors: Emacs (proced, helm-top), htop, iftop, Iotop, lsof, NetHogs, PSmisc

#### Task management: Emacs (Org-mode), Taskwarrior

* They can be used as TODO managers, calendars, etc.

* The `Taskwarrior` file format is plain text but hard to read. The editing is
far less convenient than with a proper text editor.  This is where the power of
using an editor as a user interface really shines.  Org-mode is not an Emacs
exclusivity, some other editors support it.

#### Text editors: Emacs

[Why you should use Emacs](https://www.youtube.com/watch?v=JWD1Fpdd4Pc).

#### Touch-typing: GNU Typist

#### Torrent clients: Transmission

* `Transmission` is full-featured and offers various UIs: GTK, Qt, curses,
and... Emacs!  (`transmission.el`) Beside not supporting magnet links,
`rtorrent` has a poor UI for selecting files and folders manually, which makes
it very impractical for large torrents.  The Emacs interface brings in its load
of usual advantages: extensibility, keyboard-driven, fuzzy-search, macros, etc.

#### Transcoding: cdparanoia, dvdbackup, FFmpeg, flac, Gaupol, id3v2, libvpx, MediaInfo, mkvtoolnix, opus, vorbis-tools, wavpack, x264

* `FFmpeg` is the swiss-army knife of transcoding: aspect ratio, concat, crop,
mixdown, remux, metadata, etc. It is much more efficient to use FFmpeg from a
smart custom script than using a GUI.

* `mkvtoolnix` can process mkv files in place, e.g. it can instantly change metadata.

* `cdparanoia` rips audio CDs.

* `dvdbackup` decrypts VOB files.

* `Gaupol` is a simple yet complete subtitle editor.

* `MediaInfo` displays the media property of pictures, audio and video files
(codecs, container, etc.). It overlaps a lot with `FFprobe` (from `FFmpeg`), but
still manages to provide some details that `FFprobe` misses.

* The rest is a set of tools for containers and codecs.

#### Vector graphics: Asymptote, Graphviz, Inkscape

* `Asymptote` is a full-featured descriptive vector graphics renderer. It
features libraries for: plots, trees, 3D (with perspective!), and much more. The
language is much more convenient (C-style) and far less limited than its
competitors (TikZ, Metapost, PSTricks): it has double-precision arithmetic
support, classic control structures, data structures, custom structures, etc. It
also supersedes Gnuplot.

* `Graphviz` is a smart graph drawing tool that will decide automatically of the
best arrangement for the vertices and edges.

* `Inkscape` can export in LaTeX, which is useful to improve typography or make
the font consistent in your document.

#### Version control: git, Emacs (Magit)

#### Video: mpv, subdl

* `mpv` is a fork of `mplayer` with fewer dependencies and some additions such
as an on-screen display, support for resuming and chapter markers. Both `mpv`
and `mplayer` allow for very fast video rendering, which can render 1080p
videos on lower-end machines where `VLC` would stutter.

* `subdl` will often fetch the right subtitles for the desired language. When it
fails to pick the right one, it is still possible to select it manually.

#### Virtual machine hypervisors: QEMU

#### Web browsers: Emacs (eww), qutebrowser, surfraw, w3m

* While `eww` is text-based, it can render variable width/height fonts as well
as pictures.

#### Web tools: curl, Wget, youtube-dl

* `curl` and `Wget` are overlapping but also very complementary.

* `youtube-dl`, as the name does not imply, is not restricted to YouTube.

#### Window managers: Awesome, Emacs (EXWM), i3

* `Awesome` is extensible in Lua.

* `i3` is lighter than `Awesome` but relies on externals scripts for
extensibility.

* `EXWM` is a WM where all windows are Emacs buffers.  Consequence: you can
fuzzy-select your windows, tile your selection, delete the complementary
selection, etc.  EXWM is obviously extensible in Emacs Lisp.
