This is my home page as well as a _web anti-framework_.

The framework is made of the following components:

- A _git_ repository for tracking the list of changes and modification time.

- _Markdown files_ (.md) store the actual content. Some Markdown extensions are
used. _SVG_ files can be built from _PDF_ files if needed.

- An HTML generator written in Go, using Blackfriday.

- A _Makefile_ for building the site.

# Dependencies

- Asymptote
- Git
- Go
- Make
- pdf2svg

Optional:

- LaTeX (for PDF output)

# Build

- Commit all Markdown files.
- Run `make` from the root folder to generate the entire website.
- Commit all HTML files.

The date of the article is generated from the last commit of the corresponding
Markdown. This is why you cannot commit the HTML and the .md together.

The use of `.gitattributes` will prevent git from recording the diff of the
generated files. This is why HTML files are seen as binary files.

If you run `make` before committing the .md files, HTML files will have the
wrong date. You can force re-generation with `make -B`.

To build a single article, run `make <article>/index.html`.

As an extra feature, the Makefile let's you generate PDFs:

- Run `make pdf` from the root folder to generate PDFs from a predefined list of
articles.

- Run `make <article>/index.pdf` to build PDFs individually.

# Rationale

The content generation happens offline. It is both faster to load and server
friendly, except for the global disk usage. But since it is not bloated by
undesired scripts and some web fanciness, the end result is probably more
lightweight anyway.

Offline generation does not require more maintenance than other fully dynamic
services: it is easy to rebuild and upload in one command.

This fully home-brew framework is extremely short and simple. It gives full
control over the generated content. Configuring something just requires a few
lines of code. This is like a configuration file written in a scripting
language, except that it is not limited by the wrapping framework since it _is_
the framework itself.

The result is a fast website, with no duplicate code or content, while being
fully compatible with any browser, even in text-mode.

# Inspiration

This anti-framework was inspired by [Werc](http://werc.cat-v.org/), yet taking
its philosophy to a new extremity: no JavaScript and shorter code.

It is also very similar to [sw](https://github.com/jroimartin/sw/).
