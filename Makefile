## GNU make

HTMLC ?= ./generate
HTMLFLAGS ?= -css dark.css -icon logo.png -license LICENSE.html -index index.html -pgp ambrevar.asc

.PHONY: all
all: .git/hooks/pre-commit \
	c/index.html \
	cinema/index.html \
	cinema/suicideclub.html \
	de/index.html \
	demlo/index.html \
	emacs/index.html \
	emacs-eshell/index.html \
	emacs-everywhere/index.html \
	filesystems/index.html \
	git/index.html \
	go/index.html \
	homogeneous/index.html \
	homogeneous/perspective.svg \
	homogeneous/promotion.svg \
	hsync/index.html \
	ide/index.html \
	indentation/index.html \
	index.html \
	inprogen/index.html \
	keymap/index.html \
	LICENSE.html \
	perspector/gallery.html \
	perspector/index.html \
	power-apps/index.html \
	real-time/index.html \
	vcs/index.html \
	xii/index.html

.git/hooks/pre-commit: init-hooks.sh
	sh init-hooks.sh

${HTMLC}: generate.go
	go build -o $@

%.html: %.md ${HTMLC}
	${HTMLC} ${HTMLFLAGS} -date=`git log -1 --date=short --pretty=format:'%cd' $<` $< $@

%.svg: %.pdf
	pdf2svg $< $@

%.pdf: %.asy
	cd $$(dirname "$<") && asy -f pdf $$(basename "$<") && rm $$(basename "$*")_.pbsdat texput.pbsdat

pdf:
	$(MAKE) -f Makepdf.mk
