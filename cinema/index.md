% Cinema Frivolities

An insight of Hollywood: *Writing movies for fun and profit* (Thomas Lennon & Robert Ben Garant, 2012)

# Some uncanny movies

* 12 Angry Men
* 3 Idiots
* 50/50
* A Clockwork Orange
* A History of Violence
* A.I. Artificial Intelligence
* Abre los ojos (Open Your Eyes)
* American Beauty
* Analyze This
* Atonement
* Before Midnight
* Before Sunrise
* Before Sunset
* Boyhood
* Bronson
* Buried
* Captain Fantastic
* Carnage
* Casablanca
* Catch Me If You Can
* Children of Men
* Chinjeolhan geumjassi (Lady Vengeance)
* Chugyeogja (The Chaser)
* Clerks
* Clerks II
* Cloud Atlas
* Confessions of a Dangerous Mind
* Das Experiment (The Experiment)
* Dev D
* District 9
* Don Jon
* Donnie Darko
* Eastern Promises
* Ed Wood
* El laberinto del fauno (Pan's Labyrinth)
* Election
* Eternal Sunshine of the Spotless Mind
* Everything Is Illuminated
* Eyes wide shut
* Falling Down
* Filth
* Gangs of Wasseypur
* Goksung (The Wailing)
* Gongdong gyeongbi guyeok JSA (J.S.A.: Joint Security Area)
* Groundhog Day
* Hunt for the Wilderpeople
* Il buono, il brutto, il cattivo (The Good, the Bad and the Ugly)
* In Bruges
* Jisatsu sâkuru (Suicide Club) and one interesting [decryption](suicideclub.html)
* Joheunnom nabbeunnom isanghannom (The Good, the Bad, and the Weird)
* L.A. Confidential
* Little Miss Sunshine
* Lord of War
* Madeo (Mother)
* Magnolia
* Minority Report
* Mr. Nobody
* Mullholland Dr.
* My Cousin Vinny
* Nightcrawler
* O Brother, Where Art Thou?
* Oldeuboi
* Only God Forgives
* Perfume: The Story of a Murderer
* Revolutionary Road
* Safety Not Guaranteed
* Shallow Grave
* Smokin' Aces
* Stoker
* Thank You for Smoking
* The Act of Killing
* The Ambassador
* The Breakfast Club
* The Imaginarium of Doctor Parnassus
* The Killer Inside Me
* The Man from Earth
* Three Kings
* True Romance
* Twelve Monkeys
* Up in the Air
* We Need to Talk About Kevin
* Wo hu cang long (Crouching Tiger, Hidden Dragon)
* World's Greatest Dad
* Youth

# French age certifications

(U = no restriction)

* 127 Hours: U
* 300: 12
* A History of Violence: 12
* Chugyeogja (The Chaser): 12 (with warning)
* Drive: 12
* Eastern Promises: 12
* El laberinto del fauno (Pan's Labyrinth): 12
* Eyes Wide Shut: U
* Kick-Ass: U
* Killer Joe: 12
* Waltz with Bashir: U
* Watchmen: 12

# Computers in movies

## Jurassic Park

When Lex tries to take control of one of the computers in the command centre, we
get to contemplate how she skillfully navigates the system using a (laggy) 3D
file browser. Surprisingly, this is not just an eccentricity of some movie
writer, the program, called `fsn`, actually existed by the time the movie was
written. See [Wikipedia/Fsn](https://en.wikipedia.org/wiki/Fsn).

An open source and "modern" clone called [fsv](http://fsv.sourceforge.net/) has
been developed, if you'd feel like playing Lex with your computer.

> "It's a UNIX system! I know this!"
> -- Lex Murphy

## The Social Network

There is a famous urban legend around M. Zuckerberg who purportedly "hacked" its
campus network while being intoxicated. The myth of the genius struck us one
more time. While some dark arcane of hackerism spread through the collective
mind, the movie shows a much more realistic version of the petty offense:

- His computer runs KDE3, a Unix desktop environment popular around the events
depicted in the movie.
- He uses some "wget magic". `wget` is a popular command-line network
downloader. It is ideal to write scripts for batch downloads.
- He subsequently writes some scripts to automate the queries (e.g. when limited
to 20 pictures per page).
- He fires up Emacs, and modifies some Perl script.

Despite the lack of details, all the steps happen to be mundane tasks
related to automated network queries.

## Tron: Legacy

The movie shows a (surprisingly) witty use of computers.

Around the beginning of the movie, during the meeting with all the executives,
we get a short glimpse at one terminal running an Eshell session, the Elisp
shell of Emacs. Most certainly the top of the world in Geekland...

One subliminal frame in the same scene shows a session of Tetris run from Emacs,
while the command `hanoi-unix` was just entered...

When the main character first discovers the secret room with the terminal, his
first reflex is to type the following _meaningful_ commands:

	$ whoami
	flynn

If you'd be given a computer that has kept running for years, you'd like to
know who was using it last. This is what he is checking.

	$ uname -a
	SolarOS 4.0.1 Generic_50203-02 sun4m i386
	Unknown.Unknown

`uname` is a universal command for showing the type of system the machine is
running. It appropriately shows the OS name (a cross-over between SunOS and
Solaris?), the version, the kernel (Generic...), the machine hardware name
(i386, a popular architecture by Intel), and possibly the "Unknown" hostname,
which unveils the latent mystery around the machine!

	$ login -n root
	Login incorrect
	login: backdoor
	No home directory specified in password file!
	Logging in with home=/

Now this gets a little more creative: login with the `backdoor` name is a very naive
method to hack your way toward full-privileged access to a computer!

	# bin/history
	...

`history` is usually a command embedded in the shell, but nevermind... It would
have been much more sensible to run the `history` command when logged as
`flynn`, and not as `backdoor`.

It shows the list of the last commands that were typed. Again, it totally makes
sense to do so if you want to investigate how a computer was used the last time.

Among the various history entries, we can recognize the compilation of some
program, the editing of the last will, some system status checks and finally the
execution of the Grid simulator (last command), which projects our hero to a
virtual world.


## Nmap vs. Hollywood

See [official website](https://nmap.org/movies/) for an impressive list of
movies in which Nmap gets to be a rock star!
