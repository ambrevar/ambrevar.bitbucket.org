% Pierre Neidhardt
% Simplicity is the ultimate sophistication
% Leonardo da Vinci

A collection of articles and projects around computer science, psychology and
other frivolities.

Computer related topics tend to revolve around operating systems, languages,
algorithms and computer graphics.  And Emacs.

# Articles

- [Building your own desktop environment](de/index.html)
- [C: The Dark Corners](c/index.html)
- [Cinema Frivolities](cinema/index.html)
- [Emacs Everywhere](emacs-everywhere/index.html)
- [Emacs Pro-tips](emacs/index.html)
- [Emacs: Eshell as a main shell](emacs-eshell/index.html)
- [Filesystems unraveled](filesystems/index.html)
- [Git Pro-tips](git/index.html)
- [Go: A Short Review](go/index.html)
- [Homogeneous Coordinates: A Concrete Explanation](homogeneous/index.html)
- [Indentation: A Rationale](indentation/index.html)
- [Integrated Development Environments: Debunking the Myth](ide/index.html)
- [Mastering the keyboard](keymap/index.html)
- [Power Apps](power-apps/index.html)
- [Real-Time Kernels and Scheduling](real-time/index.html)
- [Version Control System: The One Computer Skill](vcs/index.html)
- [xii.tex: Deciphering a TeX Puzzle](xii/index.html)

# Projects

- [Arch Linux](https://www.archlinux.org/) - [Trusted User](https://www.archlinux.org/people/trusted-users/#ambrevar)
	- [Official packages](https://www.archlinux.org/packages/?sort=&q=&maintainer=ambrevar&flagged=)
	- [AUR packages](https://aur.archlinux.org/packages/?K=Ambrevar&SeB=m) (versioned [here](https://github.com/ambrevar/archlinux-pkgbuilds))
	- [pacman patches](https://www.archlinux.org/pacman)
	- [Wiki edits](https://wiki.archlinux.org/index.php/User:Ambrevar)
- [Blackfriday](https://github.com/russross/blackfriday) - a Markdown processor implemented in Go (contribution to v2 rewrite)
	- [Blackfriday-LaTeX](https://github.com/ambrevar/blackfriday-latex/) - A LaTeX renderer for Blackfriday (v2 and above)
- [Demlo](demlo/index.html) - a dynamic and extensible music library organizer
- [dotfiles](https://github.com/ambrevar/dotfiles) - POSIX scripts, xkb layouts, Emacs (EXWM, Evil, Helm, mu4e, Eshell...)
- Emacs
	- [EMMS](https://savannah.gnu.org/project/memberlist.php?group=emms) contributions
	- [Evil collection](https://github.com/jojojames/evil-collection)
	- [fish completion](https://github.com/Ambrevar/emacs-fish-completion)
	- [Helm](https://github.com/emacs-helm/helm) contributions
	- [Helm-EXWM](https://github.com/emacs-helm/helm-exwm)
	- [Helm-System-Packages](https://github.com/emacs-helm/helm-system-packages)
- [hsync](hsync/index.html) - a filesystem hierarchy synchronizer
- [Inprogen](inprogen/index.html) - an inverse procedural generator
- [LaTeX Wikibook](https://en.wikibooks.org/wiki/latex) - [contributions](https://en.wikibooks.org/wiki/User:Ambrevar/LaTeX)
- [luar](https://github.com/stevedonovan/luar) - Lua reflection bindings for Go (collaborator)
- [Perspector](perspector/index.html) - a control-point-based perspective rectification tool
- [Procedural texture generator](https://github.com/ambrevar/procedural-texture-generator) - a tech demo
- [Project Euler solutions](https://github.com/ambrevar/project-euler) -
computational problems from [Project Euler](http://projecteuler.net/)
- [Wine AppDB](https://appdb.winehq.org/) - maintenance of a few games

And various contributions to open source projects on [Bitbucket](https://bitbucket.org/ambrevar) and [GitHub](https://github.com/Ambrevar).

# Resources

- [BashFAQ](http://mywiki.wooledge.org/BashFAQ) and [BashPitfalls](http://mywiki.wooledge.org/BashPitfalls)
- [Bikeshed](http://bikeshed.com/)
- [cat-v.org](http://harmful.cat-v.org/) - impertinent yet enlightening
- [ESR's homepage](http://www.catb.org/esr)
	- [The Art of Unix Programming](http://www.catb.org/esr/writings/taoup/)
	- [The Cathedral and the Bazaar](http://www.catb.org/esr/writings/cathedral-bazaar/)
- [Feynman on the scientific method](http://www.youtube.com/watch?v=EYPapE-3FRw)
- [Ken Robinson: How schools kill creativity](http://www.ted.com/talks/ken_robinson_says_schools_kill_creativity)
- [Joel Spolski's blog](http://www.joelonsoftware.com/)
- [Paul Graham's homepage](http://www.paulgraham.com/)
- [Phrack](http://phrack.org/)
- [Suckless philosophy](http://suckless.org/philosophy)
- [Tech Talk: Linux Torvalds on git](http://www.youtube.com/watch?v=4XpnKHJAok8)
- [The Arch Way](https://wiki.archlinux.org/index.php/The_Arch_Way)
- The Art or Computer Programming -- Donald E. Knuth
- The [awk](http://www.cs.princeton.edu/~bwk/btl.mirror)(1) man page, with its unrivaled informativeness/conciseness ratio
- The C Programming Language -- Brian W. Kernighan & Dennis M. Ritchie
- The Dictator's Handbook -- Bruce Bueno de Mesquita & Alastair Smith
- The Man Who Mistook His Wife for a Hat -- Oliver Sacks
- The UNIX Programming Environment -- Brian W. Kernighan & Rob Pike
- [The XY problem](http://xyproblem.info/)
- [UML: The Positive Spin](https://archive.eiffel.com/doc/manuals/technology/bmarticles/uml/page.html)
- [Yossi Kreinin's homepage](http://yosefk.com/) (and its [C++ FQA](http://yosefk.com/c++fqa/index.html))
- [You are not so smart](http://youarenotsosmart.com/)

# Contact

Although far from the blog concept, I am open to suggestions. You can post
comments or contribute to this site by sending pull requests. (See the footer.)
If this falls short, you can send me an e-mail. (See the footer.)
