% Emacs Everywhere

*This article was initially posted on
[Reddit](https://www.reddit.com/r/emacs/comments/74hetz/emacs_everywhere/).
I've revised some arguments by taking the community feedback into account.*

For many years I refrained from using Emacs everywhere because I clung to the
following Unix philosophical principle: "Make each program do one thing well."
It did not make sense to me then to use a text editor as an e-mail client or a
music library manager.  I used to favour well-established ncurses programs like
Mutt and cmus respectively.

When I started using [Eshell as my main shell](../emacs-eshell), the benefits of
the Emacs _interface_ became increasingly obvious.  Maybe my initial reasoning
was not well founded after all.  Since then I successfully moved on to using
this user-interface-on-steroids everywhere.  Looking back, it feels like I had
been missing out and wasted my time for that many years.

This realization is what leads me to write a pamphlet on the matter to help the
user community move forward out of a loop of misconception that does not help
but waste everybody's time.  I hope to help informing you with strong arguments
as to why you should or you should not use Emacs extensively.

Too often the same fallacious misconceptions stir endless debates:

> Configuring Emacs cost thousands of hours just to tweak the editor, so sending
> e-mails with Emacs is simply not within my reach.

or, even more famously

> Emacs is a great operating system, lacking only a decent editor.

While obviously sarcastic, it might be worth point out that no, Emacs' intent is
not to be an operating system.  That being said, it is true that Emacs is not
_only_ an editor.  From a broader perspective, it would be best described as
a _programmable, text-oriented user-interface_ (containing, among others, an
editor).

As such it is erroneous to discard Emacs special modes for the sole reason that
an editor should not do anything but editing text.  If you think of Emacs as a
user interface, then it covers the same ground as Qt, GTK, Tk or _curses_ and
the Unix-philosophical argument falls flat.

As a matter of fact, using Emacs to send e-mails or listen to music is usually
_much simpler_ than configuring the editor.  This is because an editor is
intrisically richer in terms of features and configuration possibilities.

Emacs might not suit everybody's needs everywhere, although I believe that more
often than not it will.  Hopefully the insights of this pamphlet will add enough
fuel to the fire to help nuance the various views held by the community.



## Emacs vs. the others

The power features Emacs offers and that are lacking in other "common"
interfaces (GTK, Qt, Tk, EFL, cocoa, curses, etc.) include:

- Everything is text, everything is searchable and copy-able.  Even better, you
can fuzzy-search anything.  Enter Helm, Ivy and others.

- It can be fully keyboard-controlled (not very common with GTK and friends),
while mouse controls are supported too (which sucks in ncurses).

- It works both in graphical and textual environments, mostly out-of-the-box.
Nevertheless you should prefer the less limited graphical Emacs: all keyboard
modifiers are supported, various font sizes can be displayed, and... pictures!

- Configuration is done in Emacs Lisp.  Which is the best language ever, as [we
all know](http://www.paulgraham.com/avg.html).  At least when it comes to
[extensibility](https://www.gnu.org/software/emacs/emacs-paper.html).  And even
if you don't agree with that, it sucks less than most of its competitors.

- Configuration, as a consequence of being Lisp, can be tracked by version
control systems.



## What Emacs really does

Now let's move on to the core of the question: Is it wise to have _everything_
run from within Emacs?

A common misconception when thinking of "Emacs as an OS" is to assume that Emacs
special modes are re-inventing the wheel.  They are not (for most of them),
Emacs and its modes focus on the _user interface_ side of things.  The backends
are almost always separate programs.  This is precisely where the Unix
philosophy still stands strong.  Using Emacs as an interface for everything is
merely equivalent to using GTK-only applications.  (Only much better,
obviously.)

As opposed to other user interfaces Emacs is a _programmable_ environment: any
structure, interface element and even code can be passed around and combined
between the different interfaces to various programs.  Consider those canonical
features:

- Buffers

- The kill-ring

- The "undo" command (or better: undo-tree)

- Macros

- Bookmarks

- Windows

- Abbreviations if that's your thing

- Snippets if that's your thing

- Completion

- Spell checking

All of them can be applied to (when it makes sense):

- Magit

- Gnus, mu4e, whatever e-mail client you prefer

- Dired, Helm-find-files

- Elfeed, Gnus again

- EMMS

- Org-mode (notes, agenda, contacts, publishing...)

And many more.

Emacs does not lure developers into reinventing the wheel, quite the opposite:
it shines at reusing and combining features in the most unexpected ways.



## The perks of Emacs as a user interface

There is more to it:

- Since Emacs can display pictures: EMMS can display album covers, e-mails can
display inline attachments.

- Configuration consistency: Bindings, color themes and other interface elements
are consistently shared across the various special modes.  No need to waste time
_syncing the different configuration files of your different programs_ (in
different configuration languages).

- Configure, extend, fix: With Emacs, everything is configurable, even what was
not foreseen by its developers.  All the Lisp source code is available at hand.
Want to add a feature?  It's usually as simple as adding a few Elisp lines to
the configuration.  Something is broken?  After reporting it upstream, you don't
have to wait for the next release, you can hot-patch the bug from your
configuration.

- Universality.  Emacs is somewhat easy to compile.  It runs on virtually all
desktop platforms you could think of.  As such, running everything from Emacs
effectively abstracts away the OS user interface, which makes it possible to use
your personal configuration on any system.  This is especially useful when you
are forced to a famous crappy OS.

- OS-independent package manager: This provides the user with cutting-edge
packages even on (rusty) conservative distributions or when the user has no
privileges.

- Flatter learning-curve for new programs: Emacs documentation system is (more or
less) consistently used among all Emacs modes, which makes the learning process
of a new mode somewhat easier.  No need to figure out where the static
documentation is (HTML page? man page?), Emacs lets you (fuzzy-)search the
configuration variables and the bindings.

- Lightweight, efficient: When I replaced all my super-lightweight _curses_
programs with their Emacs counterparts, I did not notice a significant change in
disk usage.  With the difference that ELPA packages have both the source code
and the byte-compiled files installed.  For programmers and tinkerers, having
source code at hand is a boon.  In terms of performance, graphical Emacs is not
limited by the restrictions born by terminal emulators and inflicted upon
_curses_ programs.



## Side effects and other niceties

- Another perk of using Emacs as everything is the _ubiquitous global state_ of
all Emacs special modes.  Say you want to switch transmission to turtle mode,
there is no need to switch to transmission first, you can invoke the command or
its associated binding directly, regardless of the buffers curretly under focus.
Same goes for music with EMMS, or anything else running in the background.

- If you use Eshell, you don't need that lengthy, clunky bash/zsh/fish
configuration anymore.

- Other cumbersome configurations can go: `dircolors`, `lesspipe`,
`Xresources`... Even `fzf` can be disposed of.

- No need to duplicate your dotfiles for the root user or on remote machines:
use TRAMP!

- "Apps" for web services: `debbugs` (to browse the Emacs bug tracker),
`mediawiki-mode` to browse and edit MediaWiki-based pages, `sx` to work with
StackExchange pages...


## EXWM to rule them all

_Warning: Aficionados of sparkles and glitter, behold!  EXWM is visually as
barren as Emacs gets!_

EXWM was for me the last milestone in the course of The Big Shift to a fully
Emacs-based environment.

I've been an avid user of AwesomeWM for years, but with time I grew tired of
"losing windows" among the excess of workspaces (tags in Awesome terminology) or
screens.  I wish I could have just fuzzy-searched them with `fzf` or something
similar.  I never managed to implement the idea.  Until I discovered EXWM.

EXWM has all the benefits of being Emacs-based with the particularity that all X
windows are buffers and as such are part of Emacs buffer list, which makes for
the capability to fuzzy-select windows with Helm or Ivy!  "Lost windows" belong
to the past.  When opening several windows at once, you can configure how to
display them.  (This is a [recent addition to
Helm](https://github.com/emacs-helm/helm/commit/a66849bfc008bc0082b0a97caa3ac75652f0a8ad).)
A nice use-case is to first narrow down some windows using Helm patterns,
display them all in an "Exposé" fashion, and finally select your desired windows
visually.

Concretely, I have the following bindings:

- `s-b`: `helm-mini`
- `M-a`: `helm-mark-all`
- `s-<hjkl>`: `windmove-*`

The "Exposé" binding sequence:

	s-b <filter> M-a RET
	s-<hjkl> RET

Say I have a few Wikipedia pages of which I want to want to select one which
title I do not remember:

	s-b wikipedia M-a RET

Or usually it's enough with

	s-b wk M-a RET

then

	s-l s-j RET

if if's the second window to the right then down.

Since the window management is extensible, you can write your own
`helm-window-show-buffers-function` to fine-tune your needs:

- Always display the compile buffer at the same spot with a specific height.

- If all buffers are web browser windows, pop them up in a mosaic tiling.  If
not, use a master-slave tiling.

- Behave differently depending on the number of buffers or depending on the
content or the modes.

- Keep a column of "tools" to the right (calendar, calculator, etc.).

It is so convenient to lookup buffers with EXWM+Helm that I've quit using
workspaces (a.k.a. tags) altogether in favour of a more global approach.

Maybe one the most noticeable benefit on a daily basis is that it lifts up some
weight off the _cognitive burden_ of having to manage windows both from Emacs
and from an external window manager.  With EXWM, there is no more need to
remember two sets of bindings and windowing rules, the windowing being
effectively fully centralized.  For instance I used to reserve the `super` key
for all windowing bindings with AwesomeWM; now I reserve it to all Emacs
windowing operations, and there is no need for other bindings.

Adepts of the suckless philosophy would argue that Emacs should leave all
windowing, including its own, to an external specialized program, the window
manager.  But from the perspective that Emacs is a user interface, it is not
that much of a heresy to consider that Emacs is precisely _that window manager_.

Having Emacs as a window manager has some additional benefits, namely that it is
fully aware of the content of the Emacs buffer, which allows for specializing
windowing actions depending of the different buffers.  This is much harder to do
with any other window manager.



## Examples and list of programs

- See my [home page](../) for a link to my dotfiles which implement much of these ideas.

- I keep track of my [favourite power-user applications](../power-apps), including
commandline, text-mode and Emacs equivalents.
