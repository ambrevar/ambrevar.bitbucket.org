#!/bin/sh

cat <<'ENDOFHOOK' > .git/hooks/pre-commit
#!/bin/sh

files="$(git diff --name-only --cached)"
err=0

## WARNING: We assume files have no line breaks.
while IFS= read -r i; do
	basename="${i##*/}"
	ext="${basename##*.}"
	if [ "$ext" = "md" ]; then
		html="${i%.*}.html"
		if [ -n "$(echo "$files" | grep '^'"$html"'$')" ]; then
			echo >&2 "$html"
			err=1
		fi
	fi
	done <<EOF
$(echo "$files")
EOF

[ $err -eq 1 ] && echo >&2  "HTML files must be commited after their Markdown counterpart."
exit $err
ENDOFHOOK

chmod +x .git/hooks/pre-commit
