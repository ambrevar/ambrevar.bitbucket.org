% Perspector
% A control-point-based perspective rectification tool

# About

Perspector is a perspective rectification tool.

As opposed to most competitors out there (Gimp, Photoshop, Rawtherapee), this
tool is not axis-based but rather control-point-based. This allows for much
faster processing (in human time) while obtaining more accurate results.

It was originally created out of a need for ortho-aligning a batch of facade
pictures as quickly as possible in the most convenient manner. (Understand "the
less annoying".)

The technique behind this tool is a tad more sophisticated than the usual
axis-aligned version, without being revolutionary. I developed this program as I
could not find such a thing anywhere. I believe it would be great to see it
implemented in a tool like Gimp.

# Preview

![Facade 1](1.jpg)
![Rectified facade 1](1-rect.jpg)

See more in the [gallery](gallery.html).

# Usage

Select four control points on the picture. When processed, the picture will be
transformed so that the four control points become the corners of an
axis-aligned rectangle.

# Installation

It should run out of the box on most Unix-based systems.

Fetch the [source code][dev] and read the README file for further
instructions.

# License

Perspector is under MIT license.

# Links

* [Arch Linux package (AUR)](https://aur.archlinux.org/packages/perspector/)
* [Development page][dev]: source code, bug reports

[dev]: http://github.com/ambrevar/perspector
