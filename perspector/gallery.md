% [Perspector](index.html)
% Gallery

![Facade 1](1.jpg)
![Rectified facade 1](1-rect.jpg)

![Facade 2](2.jpg)
![Rectified facade 2](2-rect.jpg)

![Facade 3](3.jpg)
![Rectified facade 3](3-rect.jpg)

![Facade 4](4.jpg)
![Rectified facade 4](4-rect.jpg)

![Facade 5](5.jpg)
![Rectified facade 5](5-rect.jpg)

![Facade 6](6.jpg)
![Rectified facade 6](6-rect.jpg)

![Facade 7](7.jpg)
![Rectified facade 7](7-rect.jpg)

